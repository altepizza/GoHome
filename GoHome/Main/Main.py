'''
Created on Jul 28, 2016

@author: Kai
'''

import telepot
import foursquare
import time
import logging
import sys
import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('config.ini')

telegramToken = config.get('Telegram', 'Token')
telegramChatID = config.getint('Telegram', 'ChatId')
swarmToken = config.get('Swarm', 'Accesstoken')
swarmVenueID = config.get('Swarm','VenueId')
frequency = config.getint('Swarm', 'CheckFrequencyInSec')
secondsIHaveToWork = config.getint('Work', 'SecondsIHaveToWork')

timeStamp = 0

logging.basicConfig(format='%(asctime)s %(message)s')
myLogger = logging.getLogger('asdf')
h1 = logging.StreamHandler(stream=sys.stdout)
h1.setLevel(logging.DEBUG)
myLogger.addHandler(h1)
myLogger.setLevel(logging.DEBUG)

def doSomething():
        
    global timeStamp
    myLogger.debug('Best timestamp: ' + str(timeStamp) + ' - ' + time.ctime(timeStamp))
    bot = telepot.Bot(telegramToken)
    
    client = foursquare.Foursquare(access_token=swarmToken)
    a = client.users.checkins()
    myCheckinsList = a[u'checkins'][u'items']
    for checkin in myCheckinsList:
        if checkin['venue']['id'] == swarmVenueID:
            if checkin[u'createdAt'] > timeStamp:
                timeStamp = checkin[u'createdAt']
                myLogger.info('Found new checkin at: ' + str(timeStamp) + ' - ' +time.ctime(timeStamp))
    feierabend = timeStamp + secondsIHaveToWork
    
    sleepy = feierabend - time.time()
    myLogger.debug('Sleeptimer: '+ str(sleepy))
    
    if sleepy > 0:
        myLogger.debug('Sending messages')
        text = 'Ankunft: ' + time.ctime(timeStamp) + '\nFeierabend: ' + time.ctime(feierabend)
        bot.sendMessage(telegramChatID, text)
        time.sleep(sleepy)
        bot.sendMessage(telegramChatID, 'MACH FEIERABEND!!!')
        myLogger.debug('sent Message')

if __name__ == '__main__':
    myLogger.info('START')
    while True:
        doSomething()
        time.sleep(frequency)
    pass